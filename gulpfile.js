const gulp = require('gulp');
const gulpConnect = require('gulp-connect');
const gulpMinifyCss = require('gulp-minify-css');
const gulpConcat = require('gulp-concat');
const gulpUglify = require('gulp-uglify');
const gulpHtmlmin = require('gulp-htmlmin');
const gulpClean = require('gulp-clean')

gulp.task('clean', function(){
  return gulp.src('dist',{
    read:false,
    allowEmpty:true
  }).pipe(gulpClean());
});

gulp.task('css-no', async function(){
  gulp.src('./src/css/*.css')
  .pipe(gulp.dest('./dist/ok/'))
  .pipe(gulpConnect.reload());
});

gulp.task('minify-css', async function(){
  gulp.src('./src/css/*.css')
  .pipe(gulpMinifyCss({
    compatibility: 'ie8'
  }))
  .pipe(gulp.dest('./dist/'))
  .pipe(gulpConnect.reload())
});

gulp.task('minify-js', async function(){
  gulp.src([
    './src/js/*.js'
  ]).pipe(gulpConcat('bundle.js'))
  .pipe(gulpUglify())
  .pipe(gulp.dest('dist'))
  .pipe(gulpConnect.reload())
});

gulp.task('minify-html', async function(){
  gulp.src('src/*.html')
  .pipe(gulpHtmlmin({
    collapseWhitespace: true
  }))
  .pipe(gulp.dest('dist'))
  .pipe(gulpConnect.reload());
});


gulp.task('server', async function(){
  gulpConnect.server({
    root:"dist",
    livereload:true
  });
});

gulp.task('watch', async function(){
  gulp.watch('./src/js/*.js', gulp.series('minify-js'))
  gulp.watch('./src/css/*.css', gulp.series('minify-css'))
  gulp.watch('./src/*.html', gulp.series('minify-html'))
  gulp.watch('./src/css/*.css', gulp.series('css-no'))
});

gulp.task('build', gulp.series('clean','minify-css', 'minify-js', 'minify-html'))

gulp.task('default', gulp.series('build', 'watch', 'server'))

